//
// Generates VLC playlist file for all .mp4 and .webm files from current page preserving their order.
//

window.start = new Date();

var allWebms = $('[class="desktop"][title*=".webm"], [class="desktop"][title*=".mp4"]').toArray()

function escapeXml(unsafe) {
    return unsafe.replace(/[<>&'"]/g, function (c) {
        switch (c) {
            case '<': return '&lt;';
            case '>': return '&gt;';
            case '&': return '&amp;';
            case '\'': return '&apos;';
            case '"': return '&quot;';
        }
    });
}

var hackfunc = function() {
	$(document).bind('keypress', function(event) {
		if (event.which === 32) {
			$("a#programatically")[0].click()
		}
	});

	var button = `<a id="programatically" href="" download="${'WEBMS - ' + $(document).find("title").text()}.xspf">Download VLC Playlist</a>`;
	$('.adminbar__cat').append(button);

	$("a#programatically").click(function() {
		var tracks = allWebms.map(function callback(x) {
			return `<track>
						<location>${x.href}</location>
						<title>${escapeXml(x.title)}</title>
					</track>`
		})

		var xml = 
`<?xml version="1.0" encoding="UTF-8"?>
<playlist xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/" version="1">
	<title>2ch webms from ${this.href}</title>
	<trackList>
${tracks.join('\n')}
	</trackList>
</playlist>`

		this.href = "data:text/plain;charset=UTF-8," + encodeURIComponent(xml);
	});
}

hackfunc()